/**
 * HttpRequest object to make HTTP requests
 * In constructor new instance of XMLHttpRequest is created
 * @constructor
 */
const HttpRequest = function () {
    this.http = new XMLHttpRequest();
};
/**
 * Base method to make HTTP requests
 * @param method - HTTP method GET | POST (just two implemented in this example)
 * @param url - HTTP requests url
 * @param callback - callback called with two params (response and error), when response ready
 * @param body - data to send
 */
HttpRequest.prototype.request = function (method, url, callback, body) {
    this.http.open(method, url);
    this.http.onreadystatechange = this.statechange.bind(this);
    this.callback = callback;
    this.http.send(body);
};
/**
 * ReadyStateChangeHandler
 */
HttpRequest.prototype.statechange = function () {
    if (this.http.readyState === 4) {
        if (this.http.status >= 200 && this.http.status <= 299) {
            this.callback(this.http.responseText); // called when OK. Just response
        } else {
            this.callback(null, this.http.responseText); // called when error. Response is null, error forwarded
        }
    }
};
/**
 * HTTP GET method
 * @param url url to make request
 * @param callback response callback
 */
HttpRequest.prototype.get = function (url, callback) {
    this.request('GET', url, callback);
};
/**
 * HTTP POST method
 * @param url url to make requests
 * @param body data to send
 * @param callback response callback
 */
HttpRequest.prototype.post = function (url, body, callback) {
    this.request('POST', url, callback, body);
};
